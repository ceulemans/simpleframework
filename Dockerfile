FROM php:8.0.11-cli-alpine

WORKDIR /var/www/html

COPY public .

ENTRYPOINT ["php", "-S", "0.0.0.0:80", "-t", "/var/www/html"]
